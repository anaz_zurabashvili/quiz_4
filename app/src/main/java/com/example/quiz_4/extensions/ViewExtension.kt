package com.example.quiz_4.extensions

import android.view.View
import com.google.android.material.snackbar.Snackbar


fun View.showSnackBar(title:String)=
    Snackbar.make(this, title, Snackbar.LENGTH_SHORT).show()

fun View.gone() = View.GONE.also { visibility = it }

fun View.visible() = View.VISIBLE.also { visibility = it }