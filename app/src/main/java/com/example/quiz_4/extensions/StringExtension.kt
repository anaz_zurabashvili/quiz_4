package com.example.quiz_4.extensions

import kotlin.math.pow

fun Int.maxNumCheck(): Boolean  = this.toDouble().pow(2.0).toInt() in 9..25
fun Int.pow(): Int  = this.toDouble().pow(2.0).toInt()
