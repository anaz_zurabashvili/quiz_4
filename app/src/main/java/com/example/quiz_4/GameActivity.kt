package com.example.quiz_4

import android.os.Bundle
import android.util.Log.d
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.quiz_4.adapters.GameAdapter
import com.example.quiz_4.databinding.ActivityGameBinding
import com.example.quiz_4.extensions.pow
import com.example.quiz_4.extensions.showSnackBar
import kotlin.math.pow

class GameActivity : AppCompatActivity() {
    private lateinit var adapter: GameAdapter
    private lateinit var binding: ActivityGameBinding
    private var gameData = mutableListOf<String>()
    private var initData = mutableListOf<String>()
    private var verticalNum: Int = 3
    private var status = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        verticalNum = (intent.extras?.get("verticalNum") ?: 3) as Int
        generateData(verticalNum)
        initRecyclerView(verticalNum)
    }

    private fun generateData(num: Int) {
//        for (i in 1..num.pow()) {
//            if (i % 2 != 0) {
//                initData.add("X")
//            } else {
//                initData.add("O")
//            }
//        }
        when (num) {
            4 -> initData = mutableListOf(
                "X", "X", "O", "X",
                "X", "O", "O", "X",
                "O", "X", "X", "X",
                "X", "X", "O", "O"
            )
            5 -> initData = mutableListOf(
                "X", "X", "O", "X", "O",
                "X", "O", "O", "X", "X",
                "O", "X", "X", "X", "X",
                "X", "X", "O", "O", "O",
                "X", "X", "O", "O", "O"
            )
            else -> initData = mutableListOf(
                "X", "X", "O",
                "O", "O", "X",
                "X", "X", "O"
            )
        }
        gameData = initData
    }

    private fun checking() = checkFirstDiagonal(gameData.chunked(verticalNum), mutableListOf()) ||
            checkSecondDiagonal(gameData.chunked(verticalNum), mutableListOf()) ||
            checkHorizontalVertical(gameData.chunked(verticalNum), mutableListOf())

    private fun checkHorizontalVertical(
        data: List<List<String>>,
        dataCheck: MutableList<String>
    ): Boolean {
        for ((i, row) in data.withIndex()) {
            if (allEqual(row)) {
                d("row", "$row")
                d("horizontal", "${allEqual(row)}")
                return true
            }
            data.forEachIndexed { j, jSize -> if (i <= j && j < jSize.size) dataCheck.add(jSize[i]) }
            if (allEqual(dataCheck)) {
                d("dataCheck", "$dataCheck")
                d("vertical", "${allEqual(dataCheck)}")
                return true
            }
        }
        return false
    }

    private fun checkFirstDiagonal(
        data: List<List<String>>,
        dataCheck: MutableList<String>
    ): Boolean {
        data.forEachIndexed { i, _ ->
            data.forEachIndexed { j, jSize -> if (i == j && i < jSize.size) dataCheck.add(data[i][j]) }
        }
        return allEqual(dataCheck)
    }

    private fun checkSecondDiagonal(
        data: List<List<String>>,
        dataCheck: MutableList<String>
    ): Boolean {
        data.forEachIndexed { i, _ ->
            data.forEachIndexed { j, _ -> if (i + j == verticalNum - 1) dataCheck.add(data[i][j]) }
        }
        return allEqual(dataCheck)
    }

    private fun allEqual(row: List<String>): Boolean = row.all { it == row[0] }

    private fun initRecyclerView(verticalNum: Int) {
        adapter = GameAdapter()
        binding.recyclerView.layoutManager = GridLayoutManager(this, verticalNum)
        binding.recyclerView.adapter = adapter
        adapter.btnRemoveClick = {
            d("status", "$status")
            if (status) {
                gameData.removeAt(it)
                adapter.delete(it)
                if (checking()) {
                    status = false
                    binding.tvStatus.text = "End Game"
                } else binding.tvStatus.text = "Game On"
            } else binding.root.showSnackBar("Restart Game")
        }
        adapter.setData(gameData)
    }

}