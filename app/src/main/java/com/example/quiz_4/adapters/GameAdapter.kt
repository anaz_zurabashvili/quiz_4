package com.example.quiz_4.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz_4.databinding.FirstUserLayoutBinding
import com.example.quiz_4.databinding.SecondUserLayoutBinding

typealias BtnRemoveClick = (position: Int) -> Unit

class GameAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val FIRST_ITEM = 10
        private const val SECOND_ITEM = 11
    }

    private val gameData = mutableListOf<String>()
    var btnRemoveClick: BtnRemoveClick? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        if (viewType == FIRST_ITEM) {
            FirstViewHolder(
                FirstUserLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            SecondViewHolder(
                SecondUserLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FirstViewHolder) {
            holder.onBind()
        } else if (holder is SecondViewHolder) {
            holder.onBind()
        }
    }

    override fun getItemCount(): Int = gameData.size

    override fun getItemViewType(position: Int): Int =
        if (gameData[position] != "X") SECOND_ITEM else FIRST_ITEM

    inner class FirstViewHolder(private val binding: FirstUserLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        fun onBind() {
            binding.root.setOnClickListener(this)
//            model = gameData[adapterPosition]
//            binding.tvEmailAddress.text = model.emailAddress
//            binding.tvFirstName.text = model.firstName
//            binding.tvLastName.text = model.lastName
//            binding.ivDelete.setOnClickListener(this)
//            binding.ivUpdate.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            btnRemoveClick?.invoke(adapterPosition)
        }
    }

    inner class SecondViewHolder(private val binding: SecondUserLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        fun onBind() {
            binding.root.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            btnRemoveClick?.invoke(adapterPosition)
        }
    }

    fun setData(gameData: MutableList<String>) {
        this.gameData.clear()
        this.gameData.addAll(gameData)
        notifyDataSetChanged()
    }

    fun delete(position: Int) {
        this.gameData.removeAt(position)
        notifyItemRemoved(position)
    }

}