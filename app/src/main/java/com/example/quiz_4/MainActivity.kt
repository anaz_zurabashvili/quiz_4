package com.example.quiz_4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import com.example.quiz_4.databinding.ActivityMainBinding
import com.example.quiz_4.extensions.maxNumCheck

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        binding.btnStart.setOnClickListener {
            val verticalNum = binding.etVerticalNumber.text.toString()
            if (validation(verticalNum)) {
                binding.tvTitle.text = "Game On"
                initRecyclerView(verticalNum.toInt())
            } else {
                binding.tvTitle.text = "Try Again"
            }
        }
    }

    private fun validation(verticalNum: String) =
        (!verticalNum.isNullOrBlank()) && verticalNum.toInt().maxNumCheck()

    private fun initRecyclerView(verticalNum: Int) {
        val intent = Intent(this, GameActivity::class.java)
        intent.putExtra("verticalNum", verticalNum)
        startActivity(intent)
    }
}